# Generated by Django 4.2.7 on 2023-11-01 19:22

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0002_alter_receipt_catergory_alter_receipt_purchaser"),
    ]

    operations = [
        migrations.RenameField(
            model_name="receipt",
            old_name="catergory",
            new_name="category",
        ),
    ]
